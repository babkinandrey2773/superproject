const form = document.getElementById('createEvent');
const title = document.getElementById('eventTitle');
const date = document.getElementById('eventDate');
const desc = document.getElementById('eventDesc');
const btn = document.querySelector('.event-form button');


function makeEventObject(title, date, desc) {
  console.log(title.value);
  console.log(date.value);
  console.log(desc.value);

  let valTitle = title.value;
  let valDate = new Date(date.value);
  let valDecs = desc.value;
 
  /* ЗАДАНИЕ
  * реализовать код функции makeEventObject
  * эта функция принимает 3 аргумента - объекты инпутов формы.
  * чтобы получить значение какого либо инпута достаточно обратиться к его свойству value
  *
  * Возвращаемое значение функции:
  * объект, в котором должны быть 3 свойства
  *   - title - это значение инпута title, каждый третий символ которого приведен в верхний регистр
  *   - daysBefore - количество дней, сколько осталось до даты события
  *   - description - значение инпута desc, в котором удалено каждое слово "типа".
  *
  * В решении нужно обязательно использовать стрелочные функции
  * */
      const upperEachThree = () => {
        
        let res = "";
        for(i=0; i<valTitle.length; i++){
          if(i % 3 === 2){
            res += valTitle[i].toUpperCase();
          }
          else{
            res += valTitle[i];
          }
        }
        return res;
      }

      const countDaysRemain = () => {
        let curDate = new Date();
        let curDateDay = curDate.getDate();
        let valDateDay = valDate.getDate();
        
        return valDateDay - curDateDay;
      }

      const tipaCheck = () => {
        if(valDecs.includes("типа")){
          return alert("true")
        }
        
      }
  return {
    title: upperEachThree(),
    daysBefore: countDaysRemain(),
    description: tipaCheck()
  };
}

btn.disabled = !title.value || !date.value || !desc.value
form.onchange = e => {
  btn.disabled = !title.value || !date.value || !desc.value
}

form.onsubmit = e => {
  e.preventDefault();
  const list = document.querySelector('.events-list');
  
  const event = makeEventObject(title, date, desc);
  
  list.insertAdjacentHTML('beforeend',`
  <div class="events-list-item">
    <p class="event-list-title">${event.title}</p>
    <p class="event-list-days">${event.daysBefore}</p>
    <p class="event-list-desc">${event.description}</p>
  </div>
  `)
};