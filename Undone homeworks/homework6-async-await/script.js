let ipCheck = "https://api.ipify.org/?format=json";
let locationCheck = "http://ip-api.com/json/";

document.querySelector(".get_ip").addEventListener("click", async function(){
    let ipGet = await fetch(ipCheck);
    let ipObj = await ipGet.json();
    let ip = ipObj.ip;
    console.log(ip); 
    let ipToSend = `${locationCheck}${ip}?fields=status,message,continent,country,countryCode,region,regionName,city,district,zip,lat,lon,timezone,isp,org,as,query`;
    let locationProceed = await fetch(ipToSend);
    let locationData = await locationProceed.json();
    console.log(locationData);  
    document.body.append(locationBlock = document.createElement("ul"));
    let {continent, country, regionName, city, district} = locationData;
    let dataToProceed = [continent,country,regionName,city,district]
    for(key of dataToProceed){
        if(key === ""){
            continue
        }
        let data = document.createElement("li");
        data.innerHTML = key;
        locationBlock.append(data);
    }

})