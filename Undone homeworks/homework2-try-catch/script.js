const books = [
    { 
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70 
    }, 
    {
     author: "Скотт Бэккер",
     name: "Воин-пророк",
    }, 
    { 
      name: "Тысячекратная мысль",
      price: 70
    }, 
    { 
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
    }, 
    {
     author: "Дарья Донцова",
     name: "Детектив на диете",
     price: 40
    },
    {
     author: "Дарья Донцова",
     name: "Дед Снегур и Морозочка",
    }
  ];


  function toList(arr, elem=document.querySelector("#root")){
    
    let result = "";
    for(key in books){
        if(!books[key].hasOwnProperty("author") || !books[key].hasOwnProperty("name") || !books[key].hasOwnProperty("price")){

          try{
            if (!books[key].author) {
              throw new Error(`Не указан автор в объекте с индексом ${key}`);
            } 
          } catch (error){
            console.log(error.message);
        }   

        try{
          if (!books[key].name) {
              throw new Error(`Не указано название в объекте с индексом ${key}`);
            }
        } catch (error){
          console.log(error.message);
        } 
        
        try{
          if (!books[key].price) {
              throw new Error(`Не указана цена в объекте с индексом ${key}`);
            }
        } catch (error){
            console.log(error.message);
        }   

            continue
        } else{
        result += ` <li> Автор: ${arr[key].author}, название: "${arr[key].name}", цена: ${arr[key].price} </li> `
        } 
    }

    return elem.insertAdjacentHTML("beforeend", `<ul>${result}</ul>`)
}

toList(books)

// try{
//     for(i=0; i<books.length; i++){
//         if (!books[i].author) {
//             throw new Error(`Не указан автор в объекте с индексом ${i}`);
//           }
//         if (!books[i].name) {
//             throw new Error(`Не указано название в объекте с индексом ${i}`);
//           }
//         if (!books[i].price) {
//             throw new Error(`Не указана цена в объекте с индексом ${i}`);
//           }
//     }
    
// } catch (error){
//     console.log(error.message);
// }  finally{
//     toList(books);
// }