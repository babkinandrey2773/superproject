let filmsSource = 'https://ajax.test-danit.com/api/swapi/films';

function sendRequest(method, url){
    return fetch(url).then(response => {
        return response.json();
    })
}

sendRequest('GET', filmsSource)
    .then(films => {
        
        films.forEach(film => {

            let filmBlock = document.createElement("div");
            filmBlock.id = film.episodeId;
            document.body.append(filmBlock);
            let filmName = document.createElement("h1");
            filmName.innerHTML = film.name;
            let charactersList = document.createElement("ul");
            let filmInfo = document.createElement("p");
            filmInfo.innerHTML = `Number of the episode: ${film.episodeId}, episode description: "${film.openingCrawl}"`;
            filmBlock.append(filmName, filmInfo);

                film.characters.forEach(characters =>{
                   sendRequest('GET', characters).then(character => {
                       let characterToList = document.createElement("li");
                       characterToList.innerHTML = character.name;
                        charactersList.append(characterToList);
                    document.getElementById(`${film.episodeId}`).querySelector("h1").insertAdjacentElement("afterend", charactersList)
                   })
                })
            })
                
            }
    )
    .catch(err => console.log(err))










