class Employee {
    constructor(name = "Unknown name", age = "Unknown age", salary = "Unknown salary") {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name() {
        return this._name;
    }

    set name(value) {
        return this._name = value;
      }

    get age() {
        if(typeof this._age !== "number"){
            return console.log("You should enter a number");
        }
        return this._age;
    }

    set age(value) {
        return this._age = value;
      }

    get salary() {
        return this._salary;
    }

    set salary(value) {
        return this._salary = value;
      }
}

class  Programmer extends Employee {
    constructor(name, age, salary, lang = "No languages") {
        super(name,age,salary);
        this.lang = lang;

        
    }
    get salary(){
        return this._salary * 3;
    }
}

let noname = new Programmer();
console.log(noname);
let Anton = new Programmer("Anton", 35, 15000, "Ukrainian, English");
console.log(Anton);
let Andrey = new Programmer("Andrey", 17, 999999, "Ukrainian, Russian, English, German");
console.log(Andrey);
console.log(Andrey.salary);
let Saribeg = new Programmer("?", "?", "?", "Russian, ?, ?, ?, ...");
console.log(Saribeg);
console.log(Saribeg.age);