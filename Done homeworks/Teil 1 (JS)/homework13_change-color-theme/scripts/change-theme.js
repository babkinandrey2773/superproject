const changeBtn = document.querySelector("#change");

changeBtn.addEventListener("click", () => {
    if(!document.body.matches(".dark-theme")){
        document.body.classList.add("dark-theme");
        localStorage.setItem('theme', 'dark');
    } else{
        document.body.classList.remove("dark-theme");
        localStorage.setItem('theme', 'light');
    }
    
})

if(localStorage.getItem('theme') === 'dark'){
    document.body.classList.add('dark-theme')
} else{
    document.body.classList.remove("dark-theme")
}

