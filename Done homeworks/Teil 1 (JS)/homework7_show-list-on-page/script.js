function toList(arr, elem=document.body){
    let newArr = arr.map(function(el){
            return `<li>${el}</li>`;        
    })
    return elem.insertAdjacentHTML("beforeend", `<ul>${newArr.join("")}</ul>`)
}

toList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv", 64546546,642, 6565532, {lol: "lol", kek: "kek"}], document.getElementById("lol"))