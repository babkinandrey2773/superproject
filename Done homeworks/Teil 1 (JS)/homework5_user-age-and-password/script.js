function createNewUser(firstName, lastName, birthday){
    let splitBday = birthday.split(".");
    let normDate = new Date(`${splitBday[1]}/${splitBday[0]}/${splitBday[2]}`); 
    
    return newUser = {
       firstName,
       lastName,
       birthday: normDate,
       
       getLogin : function(){
        let login = this.firstName[0] + this.lastName;
        return login.toLowerCase();
       },
       getAge : () => {
           let curDate = new Date();
           let age = curDate.getFullYear() - normDate.getFullYear();
           if(curDate.getMonth() < normDate.getMonth()){
               age--;
            }
           else if(curDate.getMonth() === normDate.getMonth()){
                if(curDate.getDate() < normDate.getDate()){
                    age--;
                }            
            }
           else{
                if(curDate.getDate() > normDate.getDate()){
                    age--;
                }
            }
           return age;
       },
       getPassword : () => firstName[0].toUpperCase() + lastName.toLowerCase() + normDate.getFullYear()

    }
}

console.log(createNewUser("фцвцф", "фцвфц", "фцвфц"));
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());
