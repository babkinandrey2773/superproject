let firstInput = document.getElementById("inp-1");
let secondInput = document.getElementById("inp-2");

function showPassword(){

    findElem = event.target;

    let input = findElem.previousElementSibling;

    if(findElem.matches(".icon-password")){
        
        if(input.type === "password"){
            input.type = "text";
            findElem.className = "fas fa-eye-slash icon-password";
        }
        else{
            input.type = "password";
            findElem.className = "fas fa-eye icon-password";
        }
    }
}

firstInput.addEventListener("click", showPassword);

secondInput.addEventListener("click", showPassword);

document.getElementById("pass-form").addEventListener("submit", () => {

    let wrong = document.getElementById("wrong");
    
    if(document.getElementsByClassName("input-pass")[0].value === document.getElementsByClassName("input-pass")[1].value){
        wrong.classList.remove("active");
        alert("You are welcome");
    }
    else{
        wrong.classList.add("active");
    }
    event.preventDefault();
})
