let input = document.getElementById("input");
let inputBox = document.getElementById("input-box"); 

input.addEventListener("focus", () =>{
    input.classList.add ("input-green");
})

input.addEventListener("blur", () => {
    input.classList.remove("input-green");
    if(input.value < 0){
        inputBox.insertAdjacentHTML("afterend", "Please enter correct price. ")
        input.classList.add ("input-fail");
    }
    else if(input.value === ""){
        return null
    }
    else{
        
    res = document.createElement("span");
    res.className = "span";
    res.innerHTML = `Текущая цена: ${input.value}`;

    closeBtn = document.createElement("span");
    closeBtn.className = "x";
    closeBtn.innerHTML = "✕";
    
    inputBox.before(res);
    res.append(closeBtn);

    input.classList.add("input-success");
    }
})

document.getElementById("main").addEventListener("click", function(elem){
    elem = event.target;
    if(elem.matches(".x")){
        elem.parentElement.remove();
        input.value = "";
    }
    
})
