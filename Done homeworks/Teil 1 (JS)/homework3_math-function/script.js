let getNum = pText => Number(prompt(pText, 0));
let checkNum = num => Number.isNaN(num);  

let first = getNum("Enter first number");
     while (checkNum(first)){
         first = getNum("Enter correct number");
     }
let second = getNum("Enter second number");
     while (checkNum(second)){
         second = getNum("Enter correct number");
     }
let operation = prompt("Enter operation. << + >>, << - >>, << * >>, << / >> are allowed");
    
function calc(a, b, go){
    switch (go) {
        case "+":   
            return a + b; 
        case "-":   
            return a - b; 
        case "*":   
            return a * b;
        case "/":   
            return a / b; 
        default:
            return "Error. Illegal operator"
        }
    } 

console.log(calc(first, second, operation));