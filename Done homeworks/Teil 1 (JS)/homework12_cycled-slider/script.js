const carouselSlide = document.querySelector(".images-wrapper");
const carouselImages = document.querySelectorAll(".images-wrapper img");

const stopBtn = document.querySelector("#stop");
const continueBtn = document.querySelector("#continue");

const size = carouselImages[0].clientWidth;

let goSlide;



function slide(){
    goSlide = setInterval(() =>{

        let curSlide = document.querySelector("img[data-select='selected']");

        counter = Array.from(carouselImages).indexOf(curSlide);
        
        if(carouselSlide.lastElementChild.dataset.select === "selected"){
            curSlide.dataset.select = "";
            nextSlide.dataset.select = "";
            carouselSlide.firstElementChild.dataset.select = "selected";
        }
    
        carouselSlide.style.transform = "translateX(" + (-size * counter) + 'px';
    
        curSlide.dataset.select = "";
    
        nextSlide = carouselImages[counter + 1];
    
        nextSlide.dataset.select = "selected";

        
    
        console.log(nextSlide);
    
        console.log(counter);
    }, 3000)
}


stopBtn.addEventListener("click", () => {
    clearInterval(goSlide);
    stopBtn.disabled = true;
    continueBtn.disabled = false;
});
continueBtn.addEventListener("click", () => {
    slide();
    continueBtn.disabled = true;
    stopBtn.disabled = false;
});

slide();


