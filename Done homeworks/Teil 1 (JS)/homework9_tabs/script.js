let tabs = document.getElementById("tabs");
let tabsContent = document.getElementById("tabs-content").children;
let something;

let selectedLi;
let selectedText;

tabs.onclick = function(event) {
 
     let target = event.target;
    if (target.tagName != 'LI') return target;
    

    highlight(target);
    if(selectedText){
      selectedText.classList.remove('active');
    }
    selectedText = tabsContent[target.dataset.number];
    tabsContent[target.dataset.number].classList.add("active");  
  };

  function highlight(li) {
    if (selectedLi) { 
      selectedLi.classList.remove('active');
    }
    selectedLi = li;
    selectedLi.classList.add('active');
  }

  let liElem = document.getElementById("tabs-content").children;
  